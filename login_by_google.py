
from flask_sqlalchemy import SQLAlchemy
from flask_social import Social
from flask_social.datastore import SQLAlchemyConnectionDatastore


from flask import Flask


app = Flask('main')
app.config['SECURITY_POST_LOGIN'] = '/login'

db = SQLAlchemy(app)
class Connection(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    provider_id = db.Column(db.String(255))
    provider_user_id = db.Column(db.String(255))
    access_token = db.Column(db.String(255))
    secret = db.Column(db.String(255))
    display_name = db.Column(db.String(255))
    profile_url = db.Column(db.String(512))
    image_url = db.Column(db.String(512))
    rank = db.Column(db.Integer)

Social(app, SQLAlchemyConnectionDatastore(db, Connection))