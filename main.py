from flask import Flask, url_for, session, render_template, flash, redirect
from sqlalchemy.ext.declarative import declarative_base
from flask_googlelogin import GoogleLogin
from flask_login import login_required

# You must configure these 3 values from Google APIs console
# https://code.google.com/apis/console

DEBUG = True


app = Flask(__name__)
app.config.update(
    DEBUG=True,
    SECRET_KEY='abcsdfe'
)
app.config['GOOGLE_LOGIN_CLIENT_ID'] = '41089613120-knkfjbhsk0opa87p4rc38opuk9iok3b7.apps.googleusercontent.com'
app.config['GOOGLE_LOGIN_CLIENT_SECRET'] = 'P7Gc6XXADjKycd1uB-i9Esmg'
app.config['GOOGLE_LOGIN_REDIRECT_URI'] = 'http://billicious-web.appspot.com'  # one of the Redirect URIs from Google APIs console


## http://billicious-web.appspot.com/


googlelogin = GoogleLogin(app)
base = declarative_base()

@app.route('/')
def index():
    return render_template('layout.html')

@app.route('/oauth2callback')
@googlelogin.oauth2callback
def create_or_update_user(token, userinfo, **params):

    print(userinfo['id'], userinfo['name'])
    #user = User.filter_by(google_id=userinfo['id']).first()
    # if user:
    #     user.name = userinfo['name']
    #     user.avatar = userinfo['picture']
    # else:
    #     user = User(google_id=userinfo['id'],
    #                 name=userinfo['name'],
    #                 avatar=userinfo['picture'])
    # db.session.add(user)
    # db.session.flush()
    # login_user(user)
    return redirect(url_for('index'))

@app.route('/login')
@login_required
def login():
    return render_template(googlelogin.login_url())






# @app.route('/login', methods=['GET', 'POST'])
# def login():
#     login_form = LoginForm()
#
#     if login_form.validate_on_submit():
#         flash('Login requested for OpenID="%s", remember_me=%s'
#               %(login_form.openid.data, str(login_form.remember_me.data)))
#
#         return redirect('/')
#
#     return render_template('login.html', title='Sign In', form=login_form)

if __name__ == '__main__':
    app.run()